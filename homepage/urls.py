from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns =[
	path('', views.profile, name='profil'),
	path('profile/', views.profile, name='profile'),
	path('about/', views.about, name='about'),
	path('contact/', views.contact, name='contact'),
	path('photo/', views.photo, name='photo'),
	path('kegiatan/', views.kegiatan, name='kegiatan'),
	path('tambah/', views.tambah, name='tambah'),
	path('pilih/<kegiatan_id>', views.pilih, name='pilih'),
	path('hapus/', views.hapusKegiatan, name='hapus'),
]