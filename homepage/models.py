from django.db import models

# Create your models here.
class Schedule(models.Model):
		name = models.CharField(max_length = 100)
		tanggal = models.DateField()
		tempat = models.CharField(max_length = 50)
		jam = models.TimeField()
		kategori = models.CharField(max_length=150)
		pilih = models.BooleanField(default = False)

		def __str__(self):
			return self.name
		